// GENERAL SETTING
window.sr = ScrollReveal({ reset: true });

// Custom Settings
sr.reveal('.foo-1', { 
	origin: 'left', 
	duration: 2000,
	delay: 100
	// easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'

});

sr.reveal('.foo-2', { 
  origin: 'bottom', 
  duration: 2000,
  easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', 
  // viewFactor: 0.5,
  distance: '6rem'
});

sr.reveal('.foo-3', { 
  rotate: { x: 100, y: 100, z: 0 },
  duration: 1000
});

sr.reveal('.foo-4', {
	origin: 'bottom',
	duration: 1000,
	easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',   
  	viewFactor: 0.5
});

sr.reveal('.foo-5', { 
	origin: 'bottom',	
	duration: 1000,
	viewFactor: 0.5 
});